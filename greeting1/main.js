new Vue({
	el: '#app',
	data:{
		greeting: 'Hello World!',
		user: 'Ikbal Chahyadi',
		city: 'Sukabumi',
	},
	methods: {
		changeGreeting(){
			this.greeting = this.greeting === 'Hello World!'?
			'What is up!' :
			'Hello World!';
		}
	}
});